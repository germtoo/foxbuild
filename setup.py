#!/usr/bin/env python3

import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "foxbuild",
    version = "0.0.1",
    author = "Xenia Group",
    author_email = "contact@xenialinux.com",
    description= ("Tool used to create custom RootFS for Xenia Linux"),
    license = "GPL-3",
    keywords = "immutable rootfs automation linux",
    url = "https://gitlab.com/xenia-group/foxbuild",
    packages = ['foxbuild'],
    long_description=read('README.md'),
    entry_points={
        'console_scripts': [
            'foxbuild = foxbuild.foxbuild:main',
        ],
    },
    requires = ["foxcommon"],
    classifiers=[
        "Development Satus :: 3 - Alpha",
        "Topic :: Automation"
        "License :: OSI Approved :: GPL-3"
    ],
)

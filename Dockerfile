FROM registry.hub.docker.com/gentoo/portage:latest AS portage
FROM registry.hub.docker.com/gentoo/stage3:latest
COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo
ENV CONFIG="https://gitlab.com/xenia-group/foxbuild/-/raw/main/build.toml?ref_type=heads"

RUN echo -e "xenia-tools/foxbuild **\nxenia-tools/catalyst_parser **\ndev-util/catalyst **\nnet-vpn/tailscale" > /etc/portage/package.accept_keywords/foxbuild
RUN emerge --quiet eselect-repository dev-vcs/git tailscale net-fs/sshfs
RUN eselect repository add xenia-overlay git https://gitlab.com/xenia-group/xenia-overlay.git
RUN emaint sync -r xenia-overlay
RUN touch /etc/portage/package.accept_keywords/foxbuild
RUN emerge --quiet --autounmask-continue foxbuild 

CMD wget $CONFIG -O build.toml && foxbuild -ic build.toml

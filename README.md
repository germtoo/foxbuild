# foxbuild
`foxbuild` is the build tool used to create Xenia Linux RootFS images.
With this tool, a user can build a Stage4 archive, then create a SquashFS archive from it, which they can use to boot Xenia Linux.
The tool makes use of Catalyst 4 to build the initial Stage4 archive, alongside `squashfs-tools`` to create the SquashFS archive.

## Documentation:
https://wiki.xenialinux.com/en/latest/development/create-root.html
https://wiki.xenialinux.com/en/latest/development/customise-root.html
                

## Arguments

`-i`, `--ci`: Redirects catalyst output to stdout/err.

`-n`, `--nobuild`: Sets up the build environment but doesn't build the stages.

`-c`, `--config`: Allows user to specify build options in a config file, instead of using interactive mode. If `-c` or `--config` is not set, foxbuild will start in interactive mode.

`-u`, `--update`: Updates stage3s and catalyst snapshot.

## Examples

`sudo foxbuild`: start foxbuild in interactive mode

`sudo foxbuild -ic config.toml`: start foxbuild using config file `config.toml`, and output catalyst logs to stdout/err

`sudo foxbuild -nuc config.toml`: update build environment for specified config

## Config File

The config file should look like this:

```toml
# The git repository for the catalyst files (must be in the same file structure as the one on the link below)
catalyst_repo = "https://gitlab.com/xenia-group/catalyst.git"
# The git branch to use for the catalyst files
git_branch = "unstable"
# The type of stage4 to build (this will be your spec file name without the .spec extension)
target = [systemd-plasma,systemd,systemd-arm]
```

### Breaking down the config file

#### `catalyst_repo`

This variable in the config file specifies the repository that will be used to fetch the .spec files from.

For official build images, we use the `catalyst` repo located in Xenia Group on our GitLab, that being located [here](https://gitlab.com/xenia-group/catalyst)

If you are going to use your own catalyst repo, it must be in the same file structure as our repository, the easiest way to do this is by cloning the catalyst repo and editing the files in there.

#### `git_branch`

This variable is quite self-explanatory, it specifies the branch to be used for fetching the spec files in git.

This can be quite useful when testing experimental branches / development branches, such as `unstable` in the official Xenia Linux catalyst repository.

#### `target`

This variable specifies the spec file/s to be used when building the Stage 4 root images. 

You can specify multiple by comma seperating the values.
